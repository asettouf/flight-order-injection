FROM openjdk:11-jre-stretch
COPY build/libs/flight-order-injection-0.1.war /liligo-api.war
CMD ["java", "-jar", "/liligo-api.war"]
