## Running the project (locally)

To simply run the project (in a dev environment), run `./gradlew bootRun`

## Compiling the project

To compile the project (for the production environment by default) run `./gradlew assemble`, the war will be located in
 `build/libs`.

##  Running the compiled project

You have 2 possibilities:

* Run it on the commandline (java 11 is required): `java -jar build/libs/flight-order-injection-0.1.war`
* Run the docker directly: `docker build -t liligo-api:latest . && docker run -p 8080:8080 liligo-api:latest` (a one liner could be `./gradlew assemble && sudo docker build -t liligo-api:latest . && sudo docker run -p 8080:8080 liligo-api:latest`)


## Endpoints:

You have 3 endpoints:

* POST to `/flightorders` the JSON input to be ingested (you can find an example at src/input/input.json)
* GET `/flightorders` to retrieve and see every flight order in the DB
* GET `/flightorders/listByMaxPrice?maxPrice=X` to retrieve flight order whose price is not above X.

# Notes
* Java 11 will create warnings as Grails or Groovy is using instropections in a way that won't be supported by Spring in the future
