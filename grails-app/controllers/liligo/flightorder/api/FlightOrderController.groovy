package liligo.flightorder.api

import grails.converters.JSON
import grails.rest.RestfulController

class FlightOrderController extends RestfulController {
    static responseFormats = ['json']

    FlightOrderController() {
        super(FlightOrder)
    }

    def index() {
        render FlightOrder.findAll() as JSON
    }

    def findByMaxPrice() {
        def result = "{'message': 'param maxPrice cannot be empty or not a double'}"
        def maxPrice = params.double('maxPrice')
        if (maxPrice != null){
            result = FlightOrder.findAll { pricePerPassenger < maxPrice } as JSON
        }
        render result
    }

    def save() {
        def flightOrders = FlightOrderParser.parseJson(request.JSON)
        def errorCounter = 0
        for (f in flightOrders) {
            f.save()
            errorCounter += (f.hasErrors() ? 1 : 0)
        }
        def statusMessage = "{status: There were ${errorCounter} filtered out of ${flightOrders.size()} records}"
        render statusMessage
    }


}
