package liligo.flightorder.api

class UrlMappings {

    static mappings = {
        "/flightorders"(resources: "flightOrder") {
            collection {
                '/listByMaxPrice'(controller: 'flightOrder', action: 'findByMaxPrice')
            }
        }
        get "/$controller"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
