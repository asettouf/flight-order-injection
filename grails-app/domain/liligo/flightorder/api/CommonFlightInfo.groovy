package liligo.flightorder.api

import groovy.transform.EqualsAndHashCode
import org.grails.datastore.gorm.GormEntity

/**
 * This class implements the common information shared between what's input and what's stored in the DB
 * It mainly checks that intra country flights are not saved
 */
@EqualsAndHashCode
class CommonFlightInfo implements GormEntity<CommonFlightInfo> {
    Location locationFrom
    Location locationTo
    int numberOfPassenger
    String airline

    static constraints = {
        locationTo blank: false
        locationFrom blank: false, validator: { val, CommonFlightInfo obj -> !val.country.equalsIgnoreCase(obj.locationTo.country) }
        airline blank: false
    }
}
