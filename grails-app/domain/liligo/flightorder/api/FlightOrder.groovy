package liligo.flightorder.api

import groovy.transform.EqualsAndHashCode
import org.grails.datastore.gorm.GormEntity

import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit


/**
 * TODO: Refactor this class to use only one with FlightOrderInput, simply filter bad records, and simply modify the
 * views based on the information
 *
 * This class implements what's stored in the DB
 * It checks that: flight duration is positive, price per passenger is < 1000, this is a round trip, and the minimum
 * trip time is above 7 days, before sending it to the DB.
 */
@EqualsAndHashCode
class FlightOrder implements GormEntity<FlightOrder>{
    /**
     * The max price we are interested in (in euros)
     */
    private static final PRICE_LIMIT = 1000.0
    private static final MINIMUM_DAYS_FOR_TRIP = 7

    CommonFlightInfo commonFlightInfo
    TripType tripType
    OffsetDateTime tripStart
    OffsetDateTime tripEnd
    double pricePerPassenger
    int flightDuration


    static constraints = {
        flightDuration blank: false, min: 0
        tripStart blank: false, validator: { val, FlightOrder obj -> val.compareTo(obj.tripEnd) < 0}
        pricePerPassenger range: 0.0..PRICE_LIMIT
        tripType blank: false, validator: {val -> val == TripType.ROUND_TRIP}
        tripEnd blank: false, validator: { val, FlightOrder obj -> ChronoUnit.DAYS.between(obj.tripStart.toInstant(),
                val.toInstant()) > MINIMUM_DAYS_FOR_TRIP }
    }

    static embedded = ["commonFlightInfo"]

}
