package liligo.flightorder.api

import java.time.OffsetDateTime

class FlightOrderInput {
    //ToDo Refactor this code and create a container
    OffsetDateTime inboundArrivalDate;
    OffsetDateTime inboundDepartureDate;
    OffsetDateTime outboundDepartureDate;
    OffsetDateTime outboundArrivalDate;
    int price;
    private CommonFlightInfo commonFlightInfo
}
