package liligo.flightorder.api

import groovy.transform.EqualsAndHashCode
import org.grails.datastore.gorm.GormEntity

@EqualsAndHashCode
class Location implements GormEntity<Location>{
    String country
    String city
}
