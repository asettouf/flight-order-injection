package liligo.flightorder.api;

public enum TripType {
    ROUND_TRIP, ONE_WAY
}
