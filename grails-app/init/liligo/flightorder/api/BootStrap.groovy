package liligo.flightorder.api

import grails.converters.JSON

import java.time.format.DateTimeFormatter

class BootStrap {

    def init = { servletContext ->
        JSON.registerObjectMarshaller(FlightOrder) {
            [
                    from                  : [
                            country: it.commonFlightInfo.locationFrom.country,
                            city   : it.commonFlightInfo.locationFrom.city
                    ],

                    to                    : [
                            country: it.commonFlightInfo.locationTo.country,
                            city   : it.commonFlightInfo.locationTo.city
                    ],
                    outbound              : DateTimeFormatter.ISO_INSTANT.format(it.tripStart),
                    inbound               : DateTimeFormatter.ISO_INSTANT.format(it.tripEnd),
                    tripType              : it.tripType.name(),
                    outboundFlightDuration: it.flightDuration,
                    pricePerPassenger     : it.pricePerPassenger,
                    provider              : it.commonFlightInfo.airline
            ]
        }
    }
    def destroy = {
    }
}