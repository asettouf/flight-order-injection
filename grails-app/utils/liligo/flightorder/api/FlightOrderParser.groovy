package liligo.flightorder.api


import java.time.Duration
import java.time.OffsetDateTime

/**
 * Main class used for parsing, check first that the necessary infos are into the json provided, creates an input
 * based on this, create the DB record and send it back to the controller
 */
class FlightOrderParser {

    /**
     * Parses the json sent to the POST endpoint
     * @param jsonRequest The list of elements sent
     * @return The list of FlightOrder corresponding
     */
    static def parseJson(jsonRequest){
        List<FlightOrder> flightOrders = new ArrayList<>();
        for(r in jsonRequest){
            flightOrders.add(parseOneRequest(r))
        }
        return flightOrders
    }

    /**
     * Parses one element in the list sent in JSON
     * @param jsonRequest An element in the list
     * @return The FlightOrder corresponding to the elemnt
     */
    private static def parseOneRequest(jsonRequest) {
        if (!validateJsonKeys(jsonRequest)) {
            return "{status: The json you sent was not valid}"
        }
        def locationTo = new Location(
                country: jsonRequest.to.country,
                city: jsonRequest.to.city
        )
        def locationFrom = new Location(
                country: jsonRequest.from.country,
                city: jsonRequest.from.city
        )
        def commonFlightInfo = new CommonFlightInfo(
                locationFrom: locationFrom,
                locationTo: locationTo,
                numberOfPassenger: jsonRequest.numberOfPassengers,
                airline: jsonRequest.provider

        )
        def flightOrderInput = new FlightOrderInput(
                commonFlightInfo: commonFlightInfo,
                outboundDepartureDate: OffsetDateTime.parse(jsonRequest.outbound.departure),
                outboundArrivalDate: OffsetDateTime.parse(jsonRequest.outbound.arrival),
                price: jsonRequest.price)
        if (jsonRequest.inbound) {
            flightOrderInput.inboundDepartureDate = OffsetDateTime.parse(jsonRequest.inbound.departure)
            flightOrderInput.inboundArrivalDate = OffsetDateTime.parse(jsonRequest.inbound.arrival)
        }
        return convertInputToDefaultFlightOrder(flightOrderInput)
    }

    /**
     * Convert a FlightOrderInput to a FlightOrder
     * @param flightOrderInput The input to convert
     * @return A FlightOrder corresponding
     */
    private static def convertInputToDefaultFlightOrder(FlightOrderInput flightOrderInput) {
        def flightDuration = Duration.between(flightOrderInput.outboundDepartureDate.toInstant(), flightOrderInput
                .outboundArrivalDate.toInstant()).toMinutes()
        return new FlightOrder(commonFlightInfo: flightOrderInput.commonFlightInfo,
                pricePerPassenger: flightOrderInput.price / flightOrderInput.commonFlightInfo.numberOfPassenger,
                tripStart: flightOrderInput.outboundDepartureDate,
                flightDuration: flightDuration,
                tripEnd: flightOrderInput.inboundDepartureDate != null ? flightOrderInput.inboundDepartureDate
                        : flightOrderInput.outboundArrivalDate,
                tripType: flightOrderInput.inboundArrivalDate ? TripType.ROUND_TRIP : TripType.ONE_WAY)
    }

    /**
     * Utility method to check that the json contain the keys we expect
     * @param jsonRequest An element in the list sent in JSON
     * @return A boolean indicating if the element is well formed or not
     */
    private static def validateJsonKeys(jsonRequest) {
        def isFromCountryPresent = jsonRequest.has("from") ? jsonRequest.from.has("country") : false
        def isToCountryPresent = jsonRequest.has("to") ? jsonRequest.to.has("country") : false
        def isOutBoundDepartureDatePresent = jsonRequest.has("outbound") ? jsonRequest.outbound.has("departure") : false
        def isOutBoundArrivalDatePresent = jsonRequest.has("outbound") ? jsonRequest.outbound.has("arrival") : false
        return (isFromCountryPresent && isToCountryPresent && isOutBoundArrivalDatePresent &&
                isOutBoundDepartureDatePresent
                && jsonRequest.has("numberOfPassengers") &&
                jsonRequest.has("price") && jsonRequest.has("provider"))
    }

}
