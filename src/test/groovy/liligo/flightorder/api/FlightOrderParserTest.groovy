package liligo.flightorder.api

import grails.converters.JSON

import java.time.OffsetDateTime

class FlightOrderParserTest extends GroovyTestCase {

    static mockInput = '[{"from":{"country":"Reunion","city":"Trou de Sas"},"to":{"country":"Indonesia",' +
            '"city":"Pateban"},"inbound":{"departure":"2019-08-01T18:14:21.582-03:00","arrival":"2019-08-02T00:51:21' +
            '.582+10:00"},"outbound":{"departure":"2019-07-13T18:14:21.582+10:00","arrival":"2019-07-14T00:47:21' +
            '.582-03:00"},"numberOfPassengers":2,"provider":"Qatar Airways","price":230}]'

    void testParseJson() {
        def flightOrders = FlightOrderParser.parseJson(JSON.parse(mockInput))
        def locationFromExpected = new Location(country: "Reunion", city: "Trou de Sas")
        def locationToExpected = new Location(country: "Indonesia", city: "Pateban")
        def expectedInfo = new CommonFlightInfo(locationTo: locationToExpected, locationFrom: locationFromExpected,
        numberOfPassenger: 2, airline: "Qatar Airways")
        def expected = new FlightOrder(commonFlightInfo: expectedInfo, flightDuration: 1173, tripType: TripType
                .ROUND_TRIP, pricePerPassenger: 115.0, tripStart: OffsetDateTime.parse("2019-07-13T18:14:21" +
                ".582+10:00"), tripEnd: OffsetDateTime.parse("2019-08-01T18:14:21.582-03:00"))
        assertEquals(1, flightOrders.size())
        assertEquals(expected, flightOrders.get(0))
    }
}
